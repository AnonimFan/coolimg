% coolimg(1) coolimg 1.2
% coolc
% March 2022

# NAME
coolimg - very simple image viewer

# SYNOPSIS
**coolimg** <image-file>
**coolimg** -v, --version
**coolimg** --help

# DESCRIPTION
**coolimg** is a very simple image viewer made in C with SDL2. It can display images.

# OPTIONS
**-v**, **--version**
Display version infomation.

**--help**
Display a help message.

# LICENSE
This software is released under MIT License (fun fact: it was GPLv2 before).

Check the LICENSE file for coolimg in the coolimg source directory.

There is NO WARRANTY, to the extent permitted by law.
